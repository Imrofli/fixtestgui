package org.imrofli.gui;

import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.*;
import org.imrofli.domain.FixMessage;

/**
 * Created by imrofli on 03.09.2016.
 */
public class FixInput extends CustomComponent {

    private Panel panel;
    private VerticalLayout panelContent;
    private TextArea inputArea;
    private Button submit;
    private FixOutput fixOutput;
    private FixMessage fixMessage;

    public FixInput(FixOutput fixOutput1) {
        this.fixOutput=fixOutput1;
        panel = new Panel("Enter the FixMessage to Parse");
        panelContent = new VerticalLayout();
        panelContent.setMargin(true);
        panel.setContent(panelContent);

        inputArea = new TextArea();
        inputArea.setConverter(new StringToFIXConverter());
        inputArea.setConvertedValue(fixMessage);
        inputArea.setRows(7);
        inputArea.setWidth(100.0f, Unit.PERCENTAGE);

//        inputArea.addValueChangeListener(event -> setMessage((FixMessage) inputArea.getConvertedValue()));

        submit = new Button("Parse (CTRL+ENTER)");
        submit.setWidth(7.0f, Unit.CM);
        submit.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                setMessage((FixMessage) inputArea.getConvertedValue());
            }
        });

        panelContent.addComponent(inputArea);
        panelContent.addComponent(submit);

        this.addShortcutListener(new Button.ClickShortcut(submit, ShortcutAction.KeyCode.ENTER, ShortcutAction.ModifierKey.CTRL));
        setCompositionRoot(panel);
    }

    private void setMessage(FixMessage message){
        fixOutput.setFixMessage(message);
    }


    public void setFixOutput(FixOutput fixOutput) {
        this.fixOutput = fixOutput;
    }


}
