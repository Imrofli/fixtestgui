package org.imrofli.gui;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

/**
 * Created by imrofli on 02.09.2016.
 */

@Theme("valo")
@SpringUI(path = "")
public class MainUi extends UI{

    VerticalLayout layout;

    @Override
    protected void init(VaadinRequest request) {
        layout = new VerticalLayout();
        FixOutput fixOutput = new FixOutput();
        FixInput fixInput = new FixInput(fixOutput);
        layout.addComponent(fixInput);
        layout.addComponent(fixOutput);

        setContent(layout);
    }
}
