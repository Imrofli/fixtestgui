package org.imrofli.gui;


import com.vaadin.data.Item;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import org.imrofli.domain.FixMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by imrofli on 03.09.2016.
 */

public class FixOutput extends CustomComponent {

    private static final Logger logger = LoggerFactory.getLogger(FixOutput.class);

    private FixMessage fixMessage;

    private VerticalLayout layout;
    private Table fixTable;
    private Label detailLabel;
    private Panel errorPanel;
    private VerticalLayout errorLabels;

    public FixOutput() {
        layout = new VerticalLayout();
        layout.setSizeFull();
        fixTable = new Table();
        errorPanel = new Panel("Validation Errors");
        errorPanel.setWidth(100.0f, Unit.PERCENTAGE);
        errorPanel.setVisible(false);
        errorLabels = new VerticalLayout();
        errorLabels.setWidth(100.0f, Unit.PERCENTAGE);
        errorPanel.setContent(errorLabels);
        detailLabel = new Label();
        detailLabel.setSizeFull();
        detailLabel.setContentMode(ContentMode.HTML);
        detailLabel.setImmediate(true);
        detailLabel.setWidth(100.0f, Unit.PERCENTAGE);
        fixTable.setWidth(100.0f, Unit.PERCENTAGE);
        fixTable.setSelectable(true);
        layout.addComponent(errorPanel);
        layout.addComponent(detailLabel);
        detailLabel.setVisible(false);
        layout.addComponent(fixTable);
        fixTable.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(ItemClickEvent itemClickEvent) {
                setItemSelect(itemClickEvent.getItem());
            }
        });
        setCompositionRoot(layout);
    }


    public void setFixMessage(FixMessage fixMessage) {
        this.fixMessage = fixMessage;
        fixTable.setContainerDataSource(fixMessage.getFixElementBeanContainer());
        fixTable.setVisibleColumns(new String[]{"tag", "value", "name"});
        fixTable.select(null);
        detailLabel.setVisible(false);

        for (String itemId : fixMessage.getFixElementBeanContainer().getItemIds()) {
            String tagField = (String) fixMessage.getFixElementBeanContainer().getItem(itemId).getItemProperty("tag").getValue();
            String nameField = (String) fixMessage.getFixElementBeanContainer().getItem(itemId).getItemProperty("name").getValue();
            String valueField = (String) fixMessage.getFixElementBeanContainer().getItem(itemId).getItemProperty("value").getValue();
            String descriptionField = (String) fixMessage.getFixElementBeanContainer().getItem(itemId).getItemProperty("description").getValue();
            String datatypeField = (String) fixMessage.getFixElementBeanContainer().getItem(itemId).getItemProperty("datatype").getValue();
            validateFields(tagField, nameField, valueField, descriptionField, datatypeField);
        }
    }

    private void setItemSelect(Item fixElement) {
        StringBuffer stringBuffer = new StringBuffer();
        String tagField = (String) fixElement.getItemProperty("tag").getValue();
        String nameField = (String) fixElement.getItemProperty("name").getValue();
        String valueField = (String) fixElement.getItemProperty("value").getValue();
        String descriptionField = (String) fixElement.getItemProperty("description").getValue();
        String datatypeField = (String) fixElement.getItemProperty("datatype").getValue();
        stringBuffer.append("<h4><u>" + fixElement.getItemProperty("name").getValue() + "</u></h4>");
        stringBuffer.append("<h5>Datatype: " + fixElement.getItemProperty("datatype").getValue() + "</h5>");
        stringBuffer.append("<p>" + fixElement.getItemProperty("description").getValue() + "</p>");
        decorateLabel(tagField, nameField, valueField, descriptionField, datatypeField, stringBuffer, fixElement);
        detailLabel.setValue(stringBuffer.toString());
        detailLabel.setVisible(true);
    }

    private void decorateLabel(String tagField, String nameField, String valueField, String descriptionField, String datatypeField, StringBuffer stringBuffer, Item item) {
        Integer tagInt = Integer.parseInt(tagField);
        switch (tagInt) {
            case 10:
                stringBuffer.append("<p>ISO_8859_1: " + fixMessage.checksum_ISO_8859_1 + "</p>");
                     stringBuffer.append("<p>US_ASCII:\t " + fixMessage.checksum_US_ASCII + "</p>");
                     stringBuffer.append("<p>UTF_8:\t " + fixMessage.checksum_UTF_8 + "</p>");
                     break;
        }
    }

    private void validateFields(String tagField, String nameField, String valueField, String descriptionField, String datatypeField) {
        Integer tagInt = Integer.parseInt(tagField);
        errorLabels.removeAllComponents();
        errorPanel.setVisible(false);
        switch (tagInt) {
            case 10:
                Integer msgVal = Integer.parseInt(valueField);
                if (msgVal != fixMessage.checksum_ISO_8859_1 && msgVal != fixMessage.checksum_US_ASCII && msgVal != fixMessage.checksum_UTF_8) {
                    Label tenLabel = new Label();
                    tenLabel.setContentMode(ContentMode.HTML);
                    tenLabel.setValue("<p style=\"color:red\"> the delivered checksum does not match any calculated checksum: " + msgVal + "</p>");
                    errorLabels.addComponent(tenLabel);
                    errorPanel.setVisible(true);
                }
                break;
        }
    }

}
