package org.imrofli.gui;

import com.vaadin.data.util.converter.Converter;
import org.imrofli.domain.FixMessage;
import org.imrofli.service.FixParser;

import java.util.Locale;

/**
 * Created by imrofli on 03.09.2016.
 */
public class StringToFIXConverter implements Converter<String, FixMessage> {

    private final FixParser fixParser = new FixParser();

    @Override
    public FixMessage convertToModel(String s, Class<? extends FixMessage> aClass, Locale locale) throws ConversionException {
        if (s == null) {
            return null;
        }
        FixMessage fixMessage = fixParser.parseMessage(s);
        if (fixMessage == null || fixMessage.getFixElementBeanContainer() == null) {
            throw new ConversionException("Can not convert Input to a FixMessage: "
                    + s);
        }
        return fixMessage;
    }

    @Override
    public String convertToPresentation(FixMessage fixMessage, Class<? extends String> aClass, Locale locale) throws ConversionException {
        StringBuffer stringBuffer = new StringBuffer();
        if(fixMessage==null){
            return "";
        }
        else{
            for (String itemId : fixMessage.getFixElementBeanContainer().getItemIds()) {
                String tag = (String) fixMessage.getFixElementBeanContainer().getItem(itemId).getItemProperty("tag").getValue();
                stringBuffer.append(tag);
                stringBuffer.append("=");
                stringBuffer.append((String) fixMessage.getFixElementBeanContainer().getItem(itemId).getItemProperty("value").getValue());
                if (!tag.equalsIgnoreCase("10")) {
                    stringBuffer.append(FixMessage.SEPARATOR);
                }

            }
            return stringBuffer.toString();
        }

    }

    @Override
    public Class<FixMessage> getModelType() {
        return FixMessage.class;
    }

    @Override
    public Class<String> getPresentationType() {
        return String.class;
    }
}
