package org.imrofli;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FixtestguiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FixtestguiApplication.class, args);
	}
}
