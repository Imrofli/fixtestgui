package org.imrofli.domain;

import java.io.Serializable;

/**
 * Created by imrofli on 03.09.2016.
 */
public class SingleFixTag implements Serializable{

    private Integer tag;
    private String name;
    private String description;
    private String datatype;
    private boolean standard;

    public String getDescription() {
        return description;
    }

    public Integer getTag() {
        return tag;
    }

    public void setTag(Integer tag) {
        this.tag = tag;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDatatype() {
        return datatype;
    }

    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    public boolean isStandard() {
        return standard;
    }

    public void setStandard(boolean standard) {
        this.standard = standard;
    }
}
