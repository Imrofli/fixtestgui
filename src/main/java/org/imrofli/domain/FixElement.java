package org.imrofli.domain;

/**
 * Created by imrofli on 03.09.2016.
 */
public class FixElement {
    private String tag;
    private String value;
    private SingleFixTag taginfo;

    public FixElement() {
    }

    public FixElement(String tag, String value) {
        this.tag = tag;
        this.value = value;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public SingleFixTag getTaginfo() {
        return taginfo;
    }

    public void setTaginfo(SingleFixTag taginfo) {
        this.taginfo = taginfo;
    }

    public String getName() {
        return taginfo.getName();
    }

    public String getDescription() {
        return taginfo.getDescription();
    }

    public String getDatatype() {
        return taginfo.getDatatype();

    }

    @Override
    public String toString() {
        return "FixElement{" +
                "tag='" + tag + '\'' +
                ", value='" + value + '\'' +
                ", taginfo=" + taginfo +
                '}';
    }
}
