package org.imrofli.domain;

import java.io.Serializable;
import java.util.List;

/**
 * Created by imrofli on 03.09.2016.
 */
public class FixTags implements Serializable{

    private List<SingleFixTag> fixTagList;

    public List<SingleFixTag> getFixTagList() {
        return fixTagList;
    }

    public void setFixTagList(List<SingleFixTag> fixTagList) {
        this.fixTagList = fixTagList;
    }
}
