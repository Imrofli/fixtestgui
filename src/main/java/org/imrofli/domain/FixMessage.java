package org.imrofli.domain;

import com.vaadin.data.util.BeanContainer;

/**
 * Created by imrofli on 03.09.2016.
 */
public class FixMessage {

    public final static String SEPARATOR = "|";
    public int checksum_ISO_8859_1;
    public int checksum_US_ASCII;
    public int checksum_UTF_8;
    private BeanContainer<String, FixElement> fixElementBeanContainer;


    public FixMessage() {
        fixElementBeanContainer = new BeanContainer<>(FixElement.class);
    }

    public BeanContainer<String, FixElement> getFixElementBeanContainer() {
        return fixElementBeanContainer;
    }

    public void setFixElementBeanContainer(BeanContainer<String, FixElement> fixElementBeanContainer) {
        this.fixElementBeanContainer = fixElementBeanContainer;
    }
}
