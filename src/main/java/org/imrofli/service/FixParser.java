package org.imrofli.service;

import com.vaadin.data.util.BeanContainer;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import org.imrofli.domain.FixElement;
import org.imrofli.domain.FixMessage;
import org.imrofli.domain.SingleFixTag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;

/**
 * Created by imrofli on 03.09.2016.
 */

@SpringComponent
@UIScope
public class FixParser implements Serializable{

    private static final Logger logger = LoggerFactory.getLogger(FixParser.class);

    private FixTagService fixTagService;

    private String origMessage;
    private String messageWithSeparator;
    private String fixVersion;

    public FixMessage parseMessage(String message){
        this.origMessage=message;
        fixTagService = new FixTagService();
        FixMessage fixMessage = new FixMessage();
        BeanContainer<String, FixElement> fixElements = getElements(message);
        fixMessage.setFixElementBeanContainer(fixElements);
        fixMessage.checksum_ISO_8859_1=FixHelper.CalculateChecksum(messageWithSeparator, StandardCharsets.ISO_8859_1);
        fixMessage.checksum_US_ASCII=FixHelper.CalculateChecksum(messageWithSeparator, StandardCharsets.US_ASCII);
        fixMessage.checksum_UTF_8=FixHelper.CalculateChecksum(messageWithSeparator, StandardCharsets.UTF_8);
        return fixMessage;
    }

    private BeanContainer<String, FixElement> getElements(String input) {
        logger.info("Input String: " + input);
        BeanContainer<String, FixElement> fixElements = new BeanContainer<>(FixElement.class);
        fixElements.setBeanIdProperty("tag");
        String [] toSplit = splitForTags(input);
        logger.info("splitForTags got " + toSplit.length + " Tags");
        for (String singleTag:toSplit) {
            FixElement fixElement = stringToElement(singleTag);
            if(fixElement!=null){
                fixElements.addBean(fixElement);
            }
        }
        return fixElements;
    }

    private String[] splitForTags(String s){
        String buff = s.replace("\001", FixMessage.SEPARATOR);
        this.messageWithSeparator=buff;
        logger.info("Replaced: " + buff);
        return buff.split("\\|");
    }

    private FixElement stringToElement(String s){
        logger.info("stringToElement Parsing: " + s);
        String[] split = s.split("=");
        FixElement fixElement = null;
        if(split.length>1) {
            fixElement = new FixElement(split[0], split[1]);
            if (fixElement.getTag().equalsIgnoreCase("8")) {
                fixVersion = fixElement.getValue();
            }
            matchTag(fixElement);
        }
        return fixElement;
    }

    private void matchTag(FixElement fixElement){
        SingleFixTag singleFixTag = fixTagService.getFixTag(fixVersion, fixElement.getTag());
        fixElement.setTaginfo(singleFixTag);
    }

    public FixTagService getFixTagService() {
        return fixTagService;
    }

    public void setFixTagService(FixTagService fixTagService) {
        this.fixTagService = fixTagService;
    }
}
