package org.imrofli.service;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Created by andy.boeni on 05.09.2016.
 */
public class FixHelper {

    public static int CalculateChecksum(String fixMsg, Charset charset){
        byte Separator = (byte) 124;
        int checksum=-1;
        int tagPos = fixMsg.indexOf("10=");
        if(tagPos != -1){
            fixMsg = fixMsg.substring(0, tagPos);
        }
        byte[] toCheck = fixMsg.getBytes(charset);

        for(int x=0; x < toCheck.length; x++){
            if(toCheck[x] == Separator){
                toCheck[x] = (byte) 1;
            }
        }

        int total = 0;
        for (int i = 0; i < toCheck.length; i++){
            total += toCheck[i];
        }

        checksum = total % 256;

        return checksum;

    }

}
