package org.imrofli.service;

import org.imrofli.domain.FixTags;
import org.imrofli.domain.SingleFixTag;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by andy.boeni on 06.09.2016.
 */
@Service
public class FixTagService {

    private final static String FOLDER = "fixdef/";
    Yaml yaml;
    private Map<String, List<SingleFixTag>> fixTagMap;
    private Constructor constructor;
    private TypeDescription tagDesc;


    public FixTagService() {
        constructor = new Constructor(FixTags.class);//
        tagDesc = new TypeDescription(FixTags.class);
        tagDesc.putListPropertyType("fixTagList", SingleFixTag.class);
        constructor.addTypeDescription(tagDesc);
        yaml = new Yaml(constructor);
        fixTagMap = new HashMap<>();
        loadDefs();
    }

    public SingleFixTag getFixTag(String version, String tag) {

        List<SingleFixTag> singleFixTags = fixTagMap.get(version);
        if (singleFixTags != null) {
            for (SingleFixTag singleFixTag : singleFixTags) {
                if (singleFixTag.getTag() == Integer.parseInt(tag)) {
                    return singleFixTag;
                }
            }
        }
        SingleFixTag unknown = new SingleFixTag();
        unknown.setTag(Integer.parseInt(tag));
        unknown.setDatatype("unknown");
        unknown.setName("unkown Tag, not standard?");
        unknown.setStandard(false);
        unknown.setDescription("This seems to be a non-standard tag. if you have the info, add it to the definition in " + FOLDER);
        return unknown;
    }

    private void loadDefs() {
        FileVisitor<Path> fv = new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path filePath, BasicFileAttributes attrs)
                    throws IOException {
                if (Files.isRegularFile(filePath) && filePath.toString().endsWith(".yml")) {
                    try {
                        FileInputStream fileInputStream = new FileInputStream(filePath.toFile());
                        FixTags fixTags = (FixTags) yaml.load(fileInputStream);
                        String buff = filePath.getFileName().toString();
                        String name = buff.substring(0, buff.lastIndexOf("."));
                        fixTagMap.put(name, fixTags.getFixTagList());
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                return FileVisitResult.CONTINUE;
            }
        };
        Path foldPath = Paths.get(FOLDER);
        try {
            Files.walkFileTree(foldPath, fv);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
